ggpubr_intro
========================================================
author: Eckart Bindewald
date: 05/14/2018
autosize: true

ggpubr: An R package for Simplifying Creation of High-Quality Chart
==================================

* ggpubr aims to help create "publication-ready" plots
* Based on ggplot2
* Simplified "one-command" usage
* More pleasing default font sizes and theme
* Installation from within R: `install.packages("ggpubr")`


Boxplot Example
=========

* Instead of `ggplot(...) + geom_boxplot()` use of one `ggboxplot` command
* from: <http://www.sthda.com/english/rpkgs/ggpubr/>


```r
# Load data
library(ggpubr)
data("ToothGrowth")
df <- ToothGrowth
head(df, 4)
```

```
   len supp dose
1  4.2   VC  0.5
2 11.5   VC  0.5
3  7.3   VC  0.5
4  5.8   VC  0.5
```

```r
#>    len supp dose
#> 1  4.2   VC  0.5
#> 2 11.5   VC  0.5
#> 3  7.3   VC  0.5
#> 4  5.8   VC  0.5

# Box plots with jittered points
# :::::::::::::::::::::::::::::::::::::::::::::::::::
# Change outline colors by groups: dose
# Use custom color palette
# Add jitter points and change the shape by groups
 p <- ggboxplot(df, x = "dose", y = "len")
```

Boxplot (continued)
=======================


```r
p
```

![plot of chunk unnamed-chunk-2](ggpubr_intro-figure/unnamed-chunk-2-1.png)

Customizing ggpubr Plots
==============================


```r
# Box plots with jittered points
# :::::::::::::::::::::::::::::::::::::::::::::::::::
# Change outline colors by groups: dose
# Use custom color palette
# Add jitter points and change the shape by groups
 q <- ggboxplot(df, x = "dose", y = "len",
                color = "dose", palette =c("#00AFBB", "#E7B800", "#FC4E07"),
                add = "jitter", shape = "dose")
```


Customized Boxplot (continued)
===============================


```r
print(q)
```

![plot of chunk unnamed-chunk-4](ggpubr_intro-figure/unnamed-chunk-4-1.png)

Arranging Plots with ggarrange

* Plots can be arrange in a panel with `ggarrange`
* Specify rows and columns with parameters nrows and ncols


```r
ggarrange(p,q,nrow = 1, ncol=2)
```

![plot of chunk unnamed-chunk-5](ggpubr_intro-figure/unnamed-chunk-5-1.png)

