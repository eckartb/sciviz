vizworkshop
========================================================
author:
date:
autosize: true

ggplot2 - basics
========================================================

* If ggplot2 is not installed, one can install it with:
* `install.packages("ggplot2")`

Slide With Code
========================================================
<small>

```r
library(ggplot2)
data(diamonds)
diamonds
```

```
# A tibble: 53,940 x 10
   carat cut       color clarity depth table price     x     y     z
   <dbl> <ord>     <ord> <ord>   <dbl> <dbl> <int> <dbl> <dbl> <dbl>
 1 0.230 Ideal     E     SI2      61.5   55.   326  3.95  3.98  2.43
 2 0.210 Premium   E     SI1      59.8   61.   326  3.89  3.84  2.31
 3 0.230 Good      E     VS1      56.9   65.   327  4.05  4.07  2.31
 4 0.290 Premium   I     VS2      62.4   58.   334  4.20  4.23  2.63
 5 0.310 Good      J     SI2      63.3   58.   335  4.34  4.35  2.75
 6 0.240 Very Good J     VVS2     62.8   57.   336  3.94  3.96  2.48
 7 0.240 Very Good I     VVS1     62.3   57.   336  3.95  3.98  2.47
 8 0.260 Very Good H     SI1      61.9   55.   337  4.07  4.11  2.53
 9 0.220 Fair      E     VS2      65.1   61.   337  3.87  3.78  2.49
10 0.230 Very Good H     VS1      59.4   61.   338  4.00  4.05  2.39
# ... with 53,930 more rows
```
</small>

Slide With Plot
========================================================
<small>

```
# A tibble: 234 x 11
   manufacturer model    displ  year   cyl trans   drv     cty   hwy fl   
   <chr>        <chr>    <dbl> <int> <int> <chr>   <chr> <int> <int> <chr>
 1 audi         a4        1.80  1999     4 auto(l… f        18    29 p    
 2 audi         a4        1.80  1999     4 manual… f        21    29 p    
 3 audi         a4        2.00  2008     4 manual… f        20    31 p    
 4 audi         a4        2.00  2008     4 auto(a… f        21    30 p    
 5 audi         a4        2.80  1999     6 auto(l… f        16    26 p    
 6 audi         a4        2.80  1999     6 manual… f        18    26 p    
 7 audi         a4        3.10  2008     6 auto(a… f        18    27 p    
 8 audi         a4 quat…  1.80  1999     4 manual… 4        18    26 p    
 9 audi         a4 quat…  1.80  1999     4 auto(l… 4        16    25 p    
10 audi         a4 quat…  2.00  2008     4 manual… 4        20    28 p    
# ... with 224 more rows, and 1 more variable: class <chr>
```
</small>

Scatter Plots
===================

* Adding a layer with `geom_point` results in a scatter plot


```r
ggplot(mpg, aes(x=cty,y=hwy)) + geom_point()
```

![plot of chunk unnamed-chunk-3](vizworkshop-figure/unnamed-chunk-3-1.png)

Controlling Size and Color of Plotted Elements
===============================================

* Parameter `size` controls the size of the plotted points (example: `geom_point(size=3)`)
* Parameter `colour` controls the color of the plotted elements (example: `geom_point(colour="red"))`)
* See <http://sape.inf.usi.ch/quick-reference/ggplot2/colour>

```r
ggplot(mpg, aes(x=cty,y=hwy)) + geom_point(size=4,colour="red")
```

![plot of chunk unnamed-chunk-4](vizworkshop-figure/unnamed-chunk-4-1.png)

Axis Labeles and Title
========================

* We can control axis labels with `xlab` and `ylab`.
* We can add a title with `ggtitle`


```r
ggplot(mpg, aes(x=cty,y=hwy)) + geom_point() + xlab("m/g  (City)") + ylab("m/g (Highway)") + ggtitle("Fuel Effiency")
```

![plot of chunk unnamed-chunk-5](vizworkshop-figure/unnamed-chunk-5-1.png)

Tuning Plots with Themes
====================

* We can the "theme" with predefined themes like `theme_classic()`, `theme_dark()` etc.
* We can change font sizes using themes and `base_size`: `theme_classic(base_size=24)`


```r
ggplot(mpg, aes(x=cty,y=hwy)) + geom_point() + theme_classic(base_size=24)
```

![plot of chunk unnamed-chunk-6](vizworkshop-figure/unnamed-chunk-6-1.png)

Exercise
============================

* We saw how to add labels and how to specify size, color of plot elements
* Exercise: create a scatter plot with the same data set (`mpg`) with green dots of size 4, axis labels and `theme_classic` with parameter `base_size` set to 24.
* Bonus: control semi-transparency with parameter `alpha` (a value between 0 and 1). Example: `geom_point(alpha=0.4)`. Why does this example lead to amounts of saturation for different plotted points?

Exercise
============================


```r
ggplot(mpg, aes(x=cty,y=hwy)) + geom_point(size=4,colour="green") + theme_classic(base_size=24) + xlab("City")+ ylab("Highway")
```

![plot of chunk unnamed-chunk-7](vizworkshop-figure/unnamed-chunk-7-1.png)


Other 2D Geometries: Jitter Plots
======================

* The scatter plot has the problem that points with the same x,y values are plotted at same location
* Workaround 1: use semi-transparency
* Workaround 2: use "jitter plot" with randomly created coordinate offsets (command `geom_jitter`)


```r
ggplot(mpg, aes(x=cty,y=hwy)) + geom_jitter()
```

![plot of chunk unnamed-chunk-8](vizworkshop-figure/unnamed-chunk-8-1.png)

# Discrete X, Continuous Y

Box Plots
========================

* Box-whisker plots are effective for exploratory data analysis
* Plot of minimum, 25%th percentile (quartile Q1), median, 75% percentile (quartile Q3), maximum and outliers


```r
g <- ggplot(mpg, aes(class,hwy))

g + geom_boxplot()
```

![plot of chunk unnamed-chunk-9](vizworkshop-figure/unnamed-chunk-9-1.png)

Violin Plots
========================

* Violin plots are effective for exploratory data analysis
* Plot of smoothed density (y) and categorical variable (x)


```r
g <- ggplot(mpg, aes(class,hwy))

g + geom_violin()
```

![plot of chunk unnamed-chunk-10](vizworkshop-figure/unnamed-chunk-10-1.png)

Exercises: Box-Plots and Violin Plots
======================================

* Use what we covered so far to customize the boxplot and violin plot
* Increase label font sizes, and color and size of contour lines


Arranging Plots with Faceting
==========================
* "Facetting" allows to arrange related plots in a grid
* Command `facet_grid( Y ~ X)`
Example
* Use dot (`.`) for not splitting up the data with respect to x or y


```r
t <- ggplot(mpg, aes(cty, hwy)) + geom_point()

t + facet_grid(year ~ fl)
```

![plot of chunk unnamed-chunk-11](vizworkshop-figure/unnamed-chunk-11-1.png)


Exercise: Faceting
=============================

* Try notations `. ~ fl` or `year ~ .` as arguments for `facet_grid`.
* Create a scatter plot (as before using variables cty, hwy for x and y) where the x-axis is divided into several plots based on the number of cylinders (variable `cyl`)




